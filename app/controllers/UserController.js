/**
 * @module User controller
 */

module.exports = {
    
    signin(req, res) {
        User
        .objects
        .where({ email: req.body.email })
        .fetchOne()
        .then(user => user.createToken({ password: req.body.password }).save())
        .then(user => res.json(user.token))
        .catch(error => res.status(401).json(error.code))
    },
    
    me(req, res) {
        User
        .objects
        .where({ email: req.headers.email })
        .fetchOne()
        .then(user => user.checkToken({ token: req.headers.token }) && res.json(user))
        .catch(error => res.status(401).json(error.code))
    },

    list(req, res) {
        User.objects.fetch()
        .then(users => res.json(users))
        .catch(error => res.status(401).json(error.code))
    },
    
    signup(req, res) {
        User
        .create(req.body)
        .cipherPasword()
        .save()
        .then(user => res.json(user))
        .catch(error => res.status(401).json(error.code))
    },

    update(req, res) {
        User
        .objects
        .where({ id: req.params.id })
        .update(Object.assign({ id: req.params.id }, req.body))
        .then(() => res.status(200).end())
        .catch(error => res.status(401).json(error.code))
    }

}