/**
 * @module User.js
 * @description Module gérant les utilisateurs 
 */

var bcrypt = require('bcrypt-nodejs')

module.exports = Database.model('user', {
    
    name: Database.attr(),
    email: Database.attr(),
    password: Database.attr(),
    token: Database.attr(),

    cipherPasword() {
        this.password = bcrypt.hashSync(this.password)
        return this
    },

    createToken({ password }) {
        if (!bcrypt.compareSync(password, this.password))
            return Promise.reject('BAD_PASSWORD')
        this.token = bcrypt.hashSync(this.email)
        return this
    },

    checkToken({ token }) {
        if (!bcrypt.compareSync(this.email, token))
            return Promise.reject('BAD_TOKEN')
        return this
    }

})