/**
 * @module Bootstrapper
 */

// Set environement
ENV = process.env.NODE_ENV || 'development'

module.exports = new Promise((resolve, reject) => {

    // Global useful modules
    _ = require('lodash')
    Promise = require('bluebird')
    async = require('async')

    // Configure database
    azulConfig = require('./config/azulfile')[ENV]

    // Global database connection
    Database = require('azul')(azulConfig)

    // Load services, models and controllers
    var fs = require('fs')
    async.each(
        ['./services', './app/models', './app/controllers'],
        folder => async.each(fs.readdirSync(`./${folder}/`),
            file => global[file.split('.')[0]] = require(`./${folder}/${file}`)
        )
    )

    // Load express
    var express = require('express')
    var app = express()

    // Load middlewares
    require('./config/middlewares')(app)

    // Configure routes
    async.each(require('./config/routes.js'), ([verb, path, controller, method]) => (
        app[verb](path, global[controller][method])
    ))

    app.listen(3000, function () {
        console.log(
`────────────██████████──████────
────────████▒▒░░░░░░░░██▒▒░░██──
──────██▒▒░░░░██░░██░░░░██░░░░██
────██▒▒░░░░░░██░░██░░░░░░▒▒░░██
────██░░░░░░░░██░░██░░░░░░▒▒▒▒██
──██░░░░░░▒▒▒▒░░░░░░▒▒▒▒░░░░▒▒██
██▒▒░░░░░░░░░░░░██░░░░░░░░░░░░██
██░░░░▒▒░░░░░░░░██░░░░░░░░░░▒▒██
██░░░░▒▒░░░░░░░░░░░░░░░░░░░░██──
──██████░░░░░░░░░░░░░░░░░░▒▒██──
██▒▒▒▒▒▒██░░░░░░░░░░░░░░░░▒▒██──
██▒▒▒▒▒▒▒▒██░░░░░░░░░░░░▒▒██────
██▒▒▒▒▒▒▒▒██░░░░░░░░░░▒▒████────
──██▒▒▒▒▒▒▒▒██▒▒▒▒▒▒████▒▒▒▒██──
────██▒▒▒▒██████████▒▒▒▒▒▒▒▒▒▒██
──────██████──────████████████──`
        )
        Hello.greet({ name: "Jean" }) // Hello is a service in 'services' folder with the function "greet"
        resolve(app)
    })
    
})
