// This is where we load middlewares

let bodyParser = require('body-parser')
let cookieParser = require('cookie-parser')

module.exports = (app) => {
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    app.use(cookieParser())
}

