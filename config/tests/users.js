var chai = require("chai")
var chaiAsPromised = require("chai-as-promised")
chai.use(chaiAsPromised) // https://github.com/domenic/chai-as-promised
var assert = chai.assert // http://chaijs.com/api/assert/

var request = require('superagent');

describe('Addition', function () {

    it('should compute 1 + 1', function () {
        assert.equal(1 + 1, 2)
    })
    
    it('should understand 1 + 2 does not equal 2', function () {
        assert.notEqual(1 + 2, 2)
    })
    
})

describe('Users', function () {

    var _app

    it('should start server', function (done) {

        require('../../app')

        .then((app) => {
            _app = app
            done()
        })

        .catch((err) => {
            done(err)
        })

    })

    it('should receive error 404', function () {
        
        request
        .get('http://localhost:3000/randompagerandom')
        .end(function (err, res) {
            console.log(res.status)
            assert.equal(res.status, 404)
        });

    })
    
})