module.exports = [
    [ "post", "/user/signin", "UserController", "signin" ],
    [ "post", "/user/signup", "UserController", "signup" ],
    [ "patch", "/user/:id", "UserController", "update" ],
    [ "get", "/user/me", "UserController", "me" ],
    [ "get", "/user/list", "UserController", "list" ]
]