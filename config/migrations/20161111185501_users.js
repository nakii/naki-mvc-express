exports.up = function(schema) {
  schema.createTable('users', function(table) {
    table.auto('id')
    table.string('name')
    table.string('email')
    table.string('password')
    table.string('token')
  })
}

exports.down = function(schema) {
  schema.dropTable('users')
}